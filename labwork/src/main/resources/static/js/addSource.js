var app = angular.module("addSources", []).config(function ($httpProvider) {
    csrftoken = $("meta[name='_csrf']").attr("content");
    csrfheader = $("meta[name='_csrf_header']").attr("content");
    $httpProvider.defaults.headers.common["X-CSRF-TOKEN"] = csrftoken;
});

app.controller("addSourcesController", function($scope, $http)
{
    $scope.coinSource = "";
    $scope.errorMsgSource = "";
    $scope.statusMsg = "";

    var checkSource = function()
    {
        if($scope.coinSource.length == 0)
        {
            $scope.errorMsgSource = "Должен быть не пустой строкой.";
            return false;
        }
        $scope.errorMsgSource = "";
        return true;
    };

    var onSuccess = function(response)
    {
        $scope.statusMsg = "Добавлено успешно";
    };

    var onError = function(response)
    {
        console.log(response);
        $scope.statusMsg = "Не удалось добавить источник.";
    };

    $scope.addSource = function()
    {
        if(!checkSource())
        {
            return;
        }

        var dataToRest = { coinSource: $scope.coinSource };
        var response = $http(
            {
                url: "/rest/sources",
                method: "PUT",
                params: dataToRest
            }
        );

        response.then(onSuccess, onError);
    };
});