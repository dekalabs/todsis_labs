var app = angular.module("editSource", []).config(function ($httpProvider) {
    csrftoken = $("meta[name='_csrf']").attr("content");
    csrfheader = $("meta[name='_csrf_header']").attr("content");
    $httpProvider.defaults.headers.common["X-CSRF-TOKEN"] = csrftoken;
});

app.controller("editSourceController", function($scope, $http)
{
    $scope.selectedSourceID = -1;
    $scope.sourceList = [];
    $scope.sourceAddress = "";
    $scope.errorMsgSource = "";
    $scope.errorMsgAddress = "";
    $scope.statusMsg = "";

    var onSuccessGetSources = function(response)
    {
        $scope.sourceList = response.data;
    };

    var onErrorGetSources = function(response)
    {
        console.log(response);
        $scope.statusMsg = "Внутреняя ошибка сервера: не удалось получить список источников";
    };

    $scope.getSources = function()
    {
        var response = $http(
            {
                url: "/rest/sources",
                method: "GET"
            }
        );

        response.then(onSuccessGetSources, onErrorGetSources);
    };

    var checkAddress = function()
    {
        if($scope.sourceList.length == 0)
        {
            $scope.errorMsgAddress = "Должен быть не пустой строкой.";
            return false;
        }
        $scope.errorMsgAddress = "";
        return true;
    };

    var checkSource = function()
    {
        if($scope.selectedSourceID == null || $scope.selectedSourceID < 0)
        {
            $scope.errorMsgSource = "Необходимо выбрать источник";
            return false;
        }
        else
        {
            $scope.errorMsgSource = "";
            return true;
        }
    };

    var onSuccessUpdate = function(response)
    {
        $scope.statusMsg = "Запись обновлена";
        $scope.getSources();
    };

    var onErrorUpdate = function(response)
    {
        console.log(response);
        $scope.statusMsg = "Внутреняя ошибка сервера: не удалось обновить. Убедитесь, что такого источника не существует.";
    };

    $scope.updateSource = function()
    {
        if(!checkSource())
            return;
        if(!checkAddress())
            return;

        var dataToRest = {sourceAddress: $scope.sourceAddress};
        var response = $http(
            {
                url: "/rest/sources/" + $scope.selectedSourceID,
                method: "PUT",
                params: dataToRest
            }
        );

        response.then(onSuccessUpdate, onErrorUpdate);
    };

    var onSuccessChange = function(response)
    {
        var source = response.data;
        $scope.sourceAddress = source.address;
    };

    var onErrorChange = function(response)
    {
        console.log(response);
    };

    $scope.onSourceChange = function()
    {
        if(!checkSource())
            return;

        var response = $http(
            {
                url: "/rest/sources/" + $scope.selectedSourceID,
                method: "GET"
            }
        );

        response.then(onSuccessChange, onErrorChange);
    }


    //Init
    $scope.getSources();

});