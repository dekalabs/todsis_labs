var app = angular.module("delSource", []).config(function ($httpProvider) {
    csrftoken = $("meta[name='_csrf']").attr("content");
    csrfheader = $("meta[name='_csrf_header']").attr("content");
    $httpProvider.defaults.headers.common["X-CSRF-TOKEN"] = csrftoken;
});

app.controller("delSourceController", function($scope, $http)
{
    $scope.selectedSourceID = -1;
    $scope.sourceList = [];
    $scope.errorMsgSource = "";
    $scope.statusMsg = "";

    var onSuccessGetSources = function(response)
    {
        $scope.sourceList = response.data;
    };

    var onErrorGetSources = function(response)
    {
        $scope.statusMsg = "Не удалось получить список источников монет";
    };

    $scope.getSources = function()
    {
        var response = $http(
            {
                url: "/rest/sources",
                method: "GET"
            }
        );

        response.then(onSuccessGetSources, onErrorGetSources);
    };

    var checkSource = function()
    {
        if($scope.selectedSourceID == null || $scope.selectedSourceID < 0)
        {
            $scope.errorMsgSource = "Необоходимо выбрать источник для удаления";
            return false;
        }
        else
        {
            $scope.errorMsgSource = "";
            return true;
        }
    }

    var onSuccessDel = function(response)
    {
        $scope.statusMsg = "Удалено успешно";
        $scope.getSources();
    };
    
    var onErrorDel = function(response)
    {
        console.log(response);
        $scope.statusMsg = "Не удалось удалить запись.";
    };

    $scope.delSource = function(response)
    {
        if(!checkSource())
        {
            return;
        }

        var response = $http(
            {
                url: "/rest/sources/" + $scope.selectedSourceID,
                method: "DELETE"
            }
        );

        response.then(onSuccessDel, onErrorDel);
    };


    //Init
    $scope.getSources();

});