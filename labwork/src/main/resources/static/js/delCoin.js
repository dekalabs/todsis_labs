var app = angular.module("delCoin", []).config(function ($httpProvider) {
    csrftoken = $("meta[name='_csrf']").attr("content");
    csrfheader = $("meta[name='_csrf_header']").attr("content");
    $httpProvider.defaults.headers.common["X-CSRF-TOKEN"] = csrftoken;
});

app.controller("delCoinController", function($scope, $http)
{
    $scope.selectedCoinID = -1;
    $scope.coinList = [];
    $scope.errorMsgCoin = "";
    $scope.statusMsg = "";

    var onSuccessGetCoins = function(response)
    {
        $scope.coinList = response.data;
    };

    var onErrorGetCoins = function(response)
    {
        $scope.statusMsg = "Не удалось получить список монет";
    };

    $scope.getCoins = function()
    {
        var response = $http(
            {
                url: "/rest/coins",
                method: "GET"
            }
        );

        response.then(onSuccessGetCoins, onErrorGetCoins);
    };

    var checkCoin = function()
    {
        if($scope.selectedCoinID == null || $scope.selectedCoinID < 0)
        {
            $scope.errorMsgCoin = "Необоходимо выбрать монету для удаления";
            return false;
        }
        else
        {
            $scope.errorMsgCoin = "";
            return true;
        }
    }

    var onSuccessDel = function(response)
    {
        $scope.statusMsg = "Удалено успешно";
        $scope.getCoins();
    };
    
    var onErrorDel = function(response)
    {
        console.log(response);
        $scope.statusMsg = "Не удалось удалить запись.";
    };

    $scope.delCoin = function(response)
    {
        if(!checkCoin())
        {
            return;
        }

        var response = $http(
            {
                url: "/rest/coins/" + $scope.selectedCoinID,
                method: "DELETE"
            }
        );

        response.then(onSuccessDel, onErrorDel);
    };


    //Init
    $scope.getCoins();

});