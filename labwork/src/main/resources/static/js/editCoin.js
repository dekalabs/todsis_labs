var app = angular.module("editCoin", []).config(function ($httpProvider) {
    csrftoken = $("meta[name='_csrf']").attr("content");
    csrfheader = $("meta[name='_csrf_header']").attr("content");
    console.log("token:" + csrftoken);
    console.log("header:" + csrfheader);
    $httpProvider.defaults.headers.common["X-CSRF-TOKEN"] = csrftoken;
});

app.controller("editCoinController", function($scope, $http)
{
    $scope.selectedCoinID = -1;
    $scope.coinName = "";
    $scope.errorMsgName = "";
    $scope.coinList = [];

    $scope.coinCountry = "";
    $scope.errorMsgCountry = "";

    $scope.coinYear = 0;
    $scope.errorMsgYear = "";

    $scope.statusMsg = "";

    var onSuccessGetCoins = function(response)
    {
        $scope.coinList = response.data;
    };

    var onErrorGetCoins = function(response)
    {
        console.log(response);
        $scope.statusMsg = "Внутреняя ошибка сервера: не удалось получить список монет";
    };

    $scope.getCoins = function()
    {
        var response = $http(
            {
                url: "/rest/coins",
                method: "GET"
            }
        );

        response.then(onSuccessGetCoins, onErrorGetCoins);
    };

    var onSuccessUpdate = function(response)
    {
        $scope.statusMsg = "Запись обновлена";
        $scope.getCoins();
    };

    var onErrorUpdate = function(response)
    {
        console.log(response);
        $scope.statusMsg = "Внутреняя ошибка сервера: не удалось обновить. Убедитесь, что такой монеты не существует.";
    };

    var checkCoin = function()
    {
        if($scope.selectedCoinID == null || $scope.selectedCoinID < 0)
        {
            $scope.errorMsgCoin = "Необходимо выбрать монету";
            return false;
        }
        else
        {
            $scope.errorMsgCoin = "";
            return true;
        }
    };

    var checkName = function()
    {
        if($scope.coinName.length == 0) {
            $scope.errorMsgName = "Должно быть непустой строкой.";
            return false;
        }
        $scope.errorMsgName = "";
        return true;
    };

    var checkCountry = function()
    {
        if($scope.coinCountry.length == 0) {
            $scope.errorMsgCountry = "Должно быть непустой строкой.";
            return false;
        }
        $scope.errorMsgCountry = "";
        return true;
    };

    $scope.updateCoin = function()
    {
        if(!checkCoin())
        {
            return;
        }
        if(!checkCountry() || !checkName())
        {
            return;
        }

        var dataToRest = { coinName: $scope.coinName, coinCountry: $scope.coinCountry, coinYear: $scope.coinYear };
        var response = $http(
            {
                url: "/rest/coins/" + $scope.selectedCoinID,
                method: "PUT",
                params: dataToRest
            }
        );

        response.then(onSuccessUpdate, onErrorUpdate);
    };


    var onSuccessCoinChange = function(response)
    {
        var coin = response.data;
        $scope.coinName = coin.name;
        $scope.coinCountry = coin.country;
        $scope.coinYear = coin.year;
    };

    var onErrorCoinChange = function(response)
    {
        console.log(response);
    };

    $scope.onCoinChange = function()
    {
        if(!checkCoin())
            return;
        
        var response = $http(
            {
                url: "/rest/coins/" + $scope.selectedCoinID,
                method: "GET"
            }
        );

        response.then(onSuccessCoinChange, onErrorCoinChange);
    };

    //Init
    $scope.getCoins();

});