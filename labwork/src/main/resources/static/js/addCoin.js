var app = angular.module('addCoins', []).config(function ($httpProvider) {
    csrftoken = $("meta[name='_csrf']").attr("content");
    csrfheader = $("meta[name='_csrf_header']").attr("content");
    $httpProvider.defaults.headers.common["X-CSRF-TOKEN"] = csrftoken;
});

app.controller("addCoinsController", function ($scope, $http)
{
    $scope.coinName = "";
    $scope.coinCountry = "";
    $scope.coinYear = 0;
    $scope.errorMsgName = "";
    $scope.errorMsgCountry = "";
    $scope.errorMsgYear = "";
    $scope.statusMsg = "";

    var onSuccessAdd = function(response)
    {
        $scope.statusMsg = "Успешно добавлено";
    };

    var onErrorAdd = function(response)
    {
        console.log(response);
        $scope.statusMsg = "Поля имеют не допустимые значения";
    }

    var checkName = function()
    {
        if($scope.coinName.length == 0) {
            $scope.errorMsgName = "Должно быть непустой строкой.";
            return false;
        }
        $scope.errorMsgName = "";
        return true;
    };

    var checkCountry = function()
    {
        if($scope.coinCountry.length == 0) {
            $scope.errorMsgCountry = "Должно быть непустой строкой.";
            return false;
        }
        $scope.errorMsgCountry = "";
        return true;
    };

    $scope.addCoin = function(){
        if (!checkName() || !checkCountry())
            return;

        var dataTORest = { coinName: $scope.coinName, coinCountry: $scope.coinCountry, coinYear: $scope.coinYear };
        var response = $http(
            {
                url: "/rest/coins",
                method: "PUT",
                params: dataTORest
            }
        );

        response.then(onSuccessAdd, onErrorAdd);
    };

    

});

