var app = angular.module("addConnections", []).config(function ($httpProvider) {
    csrftoken = $("meta[name='_csrf']").attr("content");
    csrfheader = $("meta[name='_csrf_header']").attr("content");
    $httpProvider.defaults.headers.common["X-CSRF-TOKEN"] = csrftoken;
});

app.controller("addConnectionsController", function ($scope, $http) {
   
    $scope.statusMsg = "";

    $scope.selectedCoinID = -1;
    $scope.errorMsgCoin = "";
    $scope.coinList = [];

    $scope.selectedSourceID = -1;
    $scope.errorMsgSource = ""
    $scope.sourceList = [];

    var rawSourceList = [];

    var onSuccessAdd = function (response) {
        $scope.statusMsg = "Добавлено успешно";
        $scope.getSources();
    };

    var onErrorAdd = function (response) {
        console.log(response);
        $scope.statusMsg = "Не удалось добавить монету к источнику.";
    };

    var onSuccessGetCoins = function (response) {
        $scope.coinList = response.data;
    };

    var onErrorGetCoins = function (response) {
        console.log(response);
        $scope.statusMsg = "Внутреняя проблема сервера: Не удалось получить список монет.";
    };

    var onSuccessGetSources = function (response) {
        rawSourceList = response.data;
        $scope.getConnection();
    };

    var onErrorGetSources = function (response) {
        console.log(response);
        $scope.statusMsg = "Внутреняя проблема сервера: Не удалось получить список источников.";
    };

    var onSuccessGetConnection = function (response) {
        $scope.selectedSourceID = -1;
        $scope.sourceList = rawSourceList;
        var connectionList = response.data;

        var filteredConnList = connectionList.filter(c => c.coin_id == $scope.selectedCoinID);

        var sourceToDelete = [];
        filteredConnList.forEach(conn => {
            $scope.sourceList.forEach(s => {
                if (s.id == conn.source_id) {
                    sourceToDelete.push(s);
                }
            });
        });

        sourceToDelete.forEach(s => {
            var id = $scope.sourceList.indexOf(s);
            if (id != -1) {
                $scope.sourceList.splice(id, 1);
            }
        });

        if ($scope.sourceList.length == 0) {
            $scope.errorMsgCoin = "Эта монета уже есть во всех источниках.";
        }
        else {
            $scope.errorMsgCoin = "";
        }

    };

    var onErrorGetConnection = function (response) {
        console.log(response);
        $scope.statusMsg = "Внутреняя проблема сервера: Не удалось получить список связей";
    };

    var checkCoin = function () {
        if ($scope.selectedCoinID == null || $scope.selectedCoinID < 0) {
            $scope.errorMsgCoin = "Необходимо указать источник.";
            return false;
        }
        else {
            $scope.errorMsgCoin = "";
            return true;
        }
    };

    var checkSource = function () {
        if ($scope.selectedSourceID == null || $scope.selectedSourceID < 0) {
            $scope.errorMsgSource = "Необходимо указать источник.";
            return false;
        }
        else {
            $scope.errorMsgSource = "";
            return true;
        }

    };

    $scope.addConnection = function () {
        if (!checkSource() || !checkCoin()) {
            return;
        }

        var dataToRest = { coin_id: $scope.selectedCoinID, source_id: $scope.selectedSourceID };
        var response = $http(
            {
                url: "/rest/connections",
                method: "PUT",
                params: dataToRest
            }
        );

        response.then(onSuccessAdd, onErrorAdd);
    };

    $scope.getCoins = function () {
        var response = $http(
            {
                url: "/rest/coins",
                method: "GET"
            }
        );

        response.then(onSuccessGetCoins, onErrorGetCoins);
    };

    $scope.getConnection = function () {
        var responseConn = $http(
            {
                url: "/rest/connections",
                method: "GET"
            }
        );

        responseConn.then(onSuccessGetConnection, onErrorGetConnection);
    };

    $scope.getSources = function () {
        var responseSource = $http(
            {
                url: "/rest/sources",
                method: "GET"
            }
        );

        responseSource.then(onSuccessGetSources, onErrorGetSources);
    };


    //INIT!
    $scope.getCoins();

});