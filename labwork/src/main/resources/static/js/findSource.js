var app = angular.module("findSource", []).config(function ($httpProvider) {
    csrftoken = $("meta[name='_csrf']").attr("content");
    csrfheader = $("meta[name='_csrf_header']").attr("content");
    $httpProvider.defaults.headers.common["X-CSRF-TOKEN"] = csrftoken;
});

app.controller("findSourceController", function($scope, $http)
{
    $scope.selectedCoinID = -1;
    $scope.coinList = [];
    $scope.results = [];

    var onSuccessGetCoins = function(response)
    {
        $scope.coinList = response.data;
    };

    var onErrorGetCoins = function(response)
    {
        console.log(response);
        $scope.statusMsg = "Внутреняя ошибка сервера: не удалось получить список монет";
    };

    $scope.getCoins = function()
    {
        var response = $http(
            {
                url: "/rest/coins",
                method: "GET"
            }
        );

        response.then(onSuccessGetCoins, onErrorGetCoins);
    };

    var checkCoin = function()
    {
        if($scope.selectedCoinID == null || $scope.selectedCoinID < 0)
        {
            $scope.errorMsgCoin = "Необходимо выбрать монету";
            return false;
        }
        else
        {
            $scope.errorMsgCoin = "";
            return true;
        }
    };

    var onSuccessCoinChange = function(response)
    {
        $scope.results = response.data;
    };

    var onErrorCoinChange = function(response)
    {
        console.log(response);
    };

    $scope.onCoinChange = function()
    {
        if(!checkCoin())
            return;
        
        var response = $http(
            {
                url: "/rest/coins/" + $scope.selectedCoinID + "/find",
                method: "GET"
            }
        );

        response.then(onSuccessCoinChange, onErrorCoinChange);
    };

    //Init
    $scope.getCoins();
});