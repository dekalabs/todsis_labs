var app = angular.module("delConnection", []).config(function ($httpProvider) {
    csrftoken = $("meta[name='_csrf']").attr("content");
    csrfheader = $("meta[name='_csrf_header']").attr("content");
    $httpProvider.defaults.headers.common["X-CSRF-TOKEN"] = csrftoken;
});

app.controller("delConnectionController", function($scope, $http)
{
    $scope.selectedCoinID = -1;
    $scope.coinList = [];
    $scope.errorMsgCoin = "";

    $scope.selectedSourceID = -1;
    $scope.sourceList = [];
    $scope.errorMsgSource = "";

    $scope.statusMsg = "";

    var rawSourceList = [];

    var onSuccessGetCoins = function(response)
    {
        $scope.coinList = response.data;
    };

    var onErrorGetCoins = function(response)
    {
        console.log(response);
        $scope.statusMsg = "Внутреняя ошибка сервера: не удалось получить список монет";
    };

    $scope.getCoins = function()
    {
        var response = $http(
            {
                url: "/rest/coins",
                method: "GET"
            }
        );

        response.then(onSuccessGetCoins, onErrorGetCoins);
    };

    var onSuccessGetConnection = function(response)
    {
        $scope.selectedSourceID = -1;
        $scope.sourceList = [];
        var connectionList = response.data;

        var filteredConnList = connectionList.filter(c => c.coin_id == $scope.selectedCoinID);

        filteredConnList.forEach(c => {
            rawSourceList.forEach(s => {
                if(s.id == c.source_id)
                {
                    $scope.sourceList.push(s);
                }
            })
        });

        if ($scope.sourceList.length == 0) {
            $scope.errorMsgCoin = "Эта монета уже недоступна во всех источниках";
        }
        else {
            $scope.errorMsgCoin = "";
        }
    };

    var onErrorGetConnection  = function(response)
    {
        console.log(response);
        $scope.statusMsg = "Внутреняя ошибка сервера: не удалось получить список связей";
    };

    $scope.getConnection = function()
    {
        var response = $http(
            {
                url: "/rest/connections",
                method: "GET"
            }
        );

        response.then(onSuccessGetConnection, onErrorGetConnection);
    }

    var onSuccessGetSources = function(response)
    {
        rawSourceList = response.data;
        $scope.getConnection();
    };

    var onErrorGetSources = function(response)
    {
        console.log(response);
        $scope.statusMsg = "Внутреняя ошибка сервера: не удалось получить список источников";
    };

    $scope.getSources = function()
    {
        var response = $http(
            {
                url: "/rest/sources",
                method: "GET"
            }
        );

        response.then(onSuccessGetSources, onErrorGetSources);
    };

    var onFinishDel = function(response)
    {
        $scope.statusMsg = "Удалено успешно.";
        $scope.getSources();
    };

    var onErrorFinishDel = function(response)
    {
        console.log(response);
        $scope.statusMsg = "Внутреняя ошибка сервера: Не удалось удалить. Возможно монета уже удалена.";
        $scope.getSources();
    };

    var finishDelete = function(conn)
    {
        var response = $http(
            {
                url: "/rest/connections/" + conn.id,
                method: "DELETE"
            }
        );

        response.then(onFinishDel, onErrorFinishDel);
    };

    var onSuccessDel = function(response)
    {
        var conn = response.data;
        finishDelete(conn);
    };

    var onErrorDel = function(response)
    {
        //Если не удалось получить, значит уже нет.
        console.log(response);
        $scope.statusMsg = "Удалено успешно.";
        $scope.getSources();
    };

    var checkCoin = function()
    {
        if($scope.selectedCoinID == null || $scope.selectedCoinID < 0)
        {
            $scope.errorMsgCoin = "Необходимо выбрать монету";
            return false;
        }
        else
        {
            $scope.errorMsgCoin = "";
            return true;
        }
    };

    var checkSource = function()
    {
        if($scope.selectedSourceID == null || $scope.selectedSourceID < 0)
        {
            $scope.errorMsgSource = "Необходимо выбрать источник";
            return false;
        }
        else
        {
            $scope.errorMsgSource = "";
            return true;
        }
    };

    $scope.delConnection = function()
    {
        if(!checkCoin() || !checkSource())
        {
            return;
        }

        var dataToRest = { coin_id: $scope.selectedCoinID, source_id: $scope.selectedSourceID }
        var response = $http(
            {
                url: "/rest/connections/get",
                method: "GET",
                params: dataToRest
            }
        );

        response.then(onSuccessDel, onErrorDel);
    }


    //Init
    $scope.getCoins();
});