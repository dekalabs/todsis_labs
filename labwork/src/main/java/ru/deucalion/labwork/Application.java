package ru.deucalion.labwork;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@ComponentScan
@EnableAutoConfiguration
@EnableWebSecurity
public class Application extends WebMvcAutoConfiguration {
    public static void main(String[] args){
        SpringApplication.run(Application.class, args);
    }
}
