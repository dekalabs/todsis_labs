package ru.deucalion.labwork.security;

import org.springframework.security.core.authority.AuthorityUtils;

import ru.deucalion.labwork.security.db.User;

public class Login extends org.springframework.security.core.userdetails.User {
	
	private static final long serialVersionUID = 1L;
	private User user;
	
	public Login(User user)
	{
		super(user.getUsername(), user.getPasshash(), AuthorityUtils.commaSeparatedStringToAuthorityList(user.getRoles().getRoleName()));
		this.user = user;
	}

	public User getUser() {
		return user;
	}

}
