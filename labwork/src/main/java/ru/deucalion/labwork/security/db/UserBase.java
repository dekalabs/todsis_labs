package ru.deucalion.labwork.security.db;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import ru.deucalion.labwork.security.Login;
import ru.deucalion.labwork.security.Roles;

@Service
public class UserBase implements UserDetailsService {
	
	@Autowired
	UserRepository repo;
    
    public boolean addUser(String username, String rawpass, Roles role)
    {
    	if(isUserExist(username))
    		return false;
    	
    	String passhash = new BCryptPasswordEncoder().encode(rawpass);
    	
    	User usr = new User();
    	usr.setId(null);
    	usr.setUsername(username);
    	usr.setPasshash(passhash);
    	usr.setRoles(role);
    	
    	repo.save(usr);
    	
    	
    	return true;
    }
    
    public User getUserByName(String username)
    {
    	Optional<User> userOpt = repo.findByUsername(username);
    	if(userOpt.isPresent())
    		return userOpt.get();
    	return null;
    }
    
    public boolean isUserExist(String username) 
    {
    	User u = getUserByName(username);
    	return u != null;
    }
	
    @Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User u = null;
		u = getUserByName(username);
		
		if(u == null)
			throw new UsernameNotFoundException("Cannot find: '" + username + "' account");
		
		Login log = new Login(u);
		return log;
	}
	
	
}
