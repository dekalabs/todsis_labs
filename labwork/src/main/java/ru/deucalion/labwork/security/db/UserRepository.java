package ru.deucalion.labwork.security.db;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Integer> {
	public Optional<User> findByUsername(String username);
}
