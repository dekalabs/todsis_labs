package ru.deucalion.labwork.security;

public enum Roles {
	USER ("USER"),
	ADMIN ("ADMIN");
	
	private String roleName;
	
	Roles(String name)
	{
		this.roleName = name;
	}
	
	public String getRoleName()
	{
		return roleName;
	}
}
