package ru.deucalion.labwork.db;

import java.util.LinkedList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DataBase {

    @Autowired
    private CoinRepository coinRepo;
    @Autowired
    private CoinSourceRepository sourceRepo;

    public LinkedList<Coin> getCoins() {
        LinkedList<Coin> coins = new LinkedList<Coin>();
        
        for (Coin coin : coinRepo.findAll())
        {
        	coins.add(coin);
        }
        
        return coins;

    }

    public LinkedList<CoinSource> getCoinSources() {
        LinkedList<CoinSource> coinSources = new LinkedList<CoinSource>();
        
        for ( CoinSource source : sourceRepo.findAll())
        {
        	coinSources.add(source);
        }

        return coinSources;
    }
    

    //Coin modification
    public boolean addCoin(String name, String country, Integer year) {
        if (coinExists(name, country, year)) {
            return false;
        }
        
        Coin coin = new Coin();
        coin.setId(null);
        coin.setName(name);
        coin.setCountry(country);
        coin.setYear(year);
        
        coinRepo.save(coin);
        
        return true;
    }

    public boolean updateCoin(Integer id, String name, String country, Integer year) {
        if (coinExistsAfterUpdate(id, name, country, year)) {
            return false;
        }
        
        Optional<Coin> editedCoinOpt = coinRepo.findById(id);
        if(editedCoinOpt.isEmpty())
        	return false;
        
        Coin coin = editedCoinOpt.get();

        if (name != null && !name.isBlank()) {
        	coin.setName(name);
        }

        if (country != null && !country.isBlank()) {
            coin.setCountry(country);
        }

        if (year != null) {
            coin.setYear(year);
        }

        return true;
    }

    public void deleteCoin(Integer id) {
        coinRepo.deleteById(id);
    }

    public Coin getCoin(Integer id)  {
        Optional<Coin> coin = coinRepo.findById(id);
        if(coin.isPresent())
        {
        	return coin.get();
        }
        return null;
    }

    public Coin getCoin(String name, String country, Integer year)  {
    	Optional<Coin> coin = coinRepo.findByNameAndCountryAndYear(name, country, year);
        if(coin.isPresent())
        {
        	return coin.get();
        }
        return null;
    }

    public boolean coinExists(String name, String country, Integer year)  {
        return getCoin(name, country, year) != null;
    }

    public boolean coinExistsAfterUpdate(Integer id, String name, String country, Integer year) {
        Coin c = getCoin(id);
        if (name != null && !name.isBlank()) {
            c.setName(name);
        }

        if (country != null && !country.isBlank()) {
            c.setCountry(country);
        }

        if (year != null) {
            c.setYear(year);
        }

        Coin possibledub = getCoin(c.getName(), c.getCountry(), c.getYear());

        if (possibledub != null && possibledub.getId() != c.getId()) {
            return true;
        }
        return false;
    }

    //Source modification
    public boolean addCoinSource(String address) {
        if (coinSourceExists(address)) {
            return false;
        }
        
        CoinSource source = new CoinSource();
        source.setId(null);
        source.setAddress(address);
        
        sourceRepo.save(source);
        
        return true;
    }

    public boolean updateCoinSource(Integer id, String address) {
        if (coinSourceExistAfterUpdate(id, address)) {
            return false;
        }
        
        Optional<CoinSource> editedSourceOpt = sourceRepo.findById(id);
        if(editedSourceOpt.isEmpty())
        	return false;
        
        CoinSource source = editedSourceOpt.get();

        if (address != null && !address.isBlank()) {
            source.setAddress(address);
        }
        
        sourceRepo.save(source);

        return true;
    }

    public void deleteCoinSource(Integer id) {
        sourceRepo.deleteById(id);
    }

    public CoinSource getCoinSource(Integer id) {
        Optional<CoinSource> source = sourceRepo.findById(id);
        if(source.isPresent())
        	return source.get();
        return null;
    }

    public CoinSource getCoinSource(String address) {
    	Optional<CoinSource> source = sourceRepo.findByAddress(address);
        if(source.isPresent())
        	return source.get();
        return null;
    }

    public boolean coinSourceExists(String address) {
        return getCoinSource(address) != null;
    }

    public boolean coinSourceExistAfterUpdate(Integer id, String address) {
        CoinSource s = getCoinSource(id);

        if (address != null && !address.isBlank()) {
            s.setAddress(address);
        }

        CoinSource possibledub = getCoinSource(s.getAddress());

        if (possibledub != null && possibledub.getId() != s.getId()) {
            return true;
        }
        return false;

    }

    //Connection modification
    public boolean addConnection(Integer coin_id, Integer source_id) {     
        Optional<Coin> coinOpt = coinRepo.findById(coin_id);
        Optional<CoinSource> sourceOpt = sourceRepo.findById(source_id);
        
        if(coinOpt.isEmpty() || sourceOpt.isEmpty())
        	return false;
        
        Coin coin = coinOpt.get();
        CoinSource source = sourceOpt.get();
        
        coin.addCoinSource(source);
        source.addCoin(coin);
        
        coinRepo.save(coin);
        sourceRepo.save(source);
        
        return true;
    }

    public void deleteConnection(Integer coin_id, Integer source_id) {
    	Optional<Coin> coinOpt = coinRepo.findById(coin_id);
        Optional<CoinSource> sourceOpt = sourceRepo.findById(source_id);
        
        if(coinOpt.isEmpty() || sourceOpt.isEmpty())
        	return;
        
        Coin coin = coinOpt.get();
        CoinSource source = sourceOpt.get();
        
        coin.delCoinSource(source);
        source.delCoin(coin);
        
        coinRepo.save(coin);
        sourceRepo.save(source);
    }

    public LinkedList<CoinSource> getSourcesForCoin(Integer coin_id) {
    	LinkedList<CoinSource> sources = new LinkedList<CoinSource>();
    	
        Coin coin = getCoin(coin_id);
        if(coin == null)
        	return sources;
        
        
        for ( CoinSource source : coin.getSourceConnected() )
        {
        	sources.add(source);
        }
        
        return sources;
    }

}
