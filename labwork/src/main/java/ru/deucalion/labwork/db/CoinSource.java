package ru.deucalion.labwork.db;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "source")
public class CoinSource {
	@Id
	@Column(name = "id", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "address", nullable = false)
	private String address;
	
	@ManyToMany(targetEntity = Coin.class)
	private Set<Coin> coinConnected;
	
	
	public Set<Coin> getCoinConnected() {
		return coinConnected;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	public void addCoin(Coin coin)
	{
		if(coinConnected.contains(coin))
			return;
		coinConnected.add(coin);
	}
	public void delCoin(Coin coin)
	{
		coinConnected.remove(coin);
	}
	
	public static boolean validateAddress(String address)
	{
		if(address == null)
			return false;
		if(address.isBlank())
			return false;
		if(address.length() > 150)
			return false;
		return true;
	}
	
	

}
