package ru.deucalion.labwork.db;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "coin")
public class Coin {
	
	@Id
	@Column(name = "id", nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "name", nullable = false, length = 150)
	private String name;
	
	@Column(name = "country", nullable = false, length = 150)
	private String country;
	
	@Column(name = "year", nullable = false)
	private Integer year;
	
	@ManyToMany(targetEntity = CoinSource.class)
	private Set<CoinSource> sourceConnected;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	
	public void addCoinSource(CoinSource source)
	{
		if(sourceConnected.contains(source))
			return;
		sourceConnected.add(source);
	}
	public void delCoinSource(CoinSource source)
	{
		sourceConnected.remove(source);
	}
	
	
	public Set<CoinSource> getSourceConnected() {
		return sourceConnected;
	}
	static public boolean validateName(String name)
	{
		if(name == null)
			return false;
		if(name.isBlank())
			return false;
		if(name.length() > 150)
			return false;
		return true;
	}
	
	static public boolean validateCountry(String country)
	{
		if(country == null)
			return false;
		if(country.isBlank())
			return false;
		if(country.length() > 150)
			return false;
		return true;
	}
	
	static public boolean validateYear(String year)
	{
		if (year == null)
			return false;

		try {
			Integer.parseInt(year);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

}
