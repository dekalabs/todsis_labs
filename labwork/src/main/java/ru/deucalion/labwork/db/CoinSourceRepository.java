package ru.deucalion.labwork.db;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

public interface CoinSourceRepository extends CrudRepository<CoinSource, Integer> {

	public Optional<CoinSource> findByAddress(String address);
}
