package ru.deucalion.labwork.db;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

public interface CoinRepository extends CrudRepository<Coin, Integer> {

	public Optional<Coin> findByNameAndCountryAndYear(String name, String country, Integer year);
}
