package ru.deucalion.labwork.config;

import java.util.Locale;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

@Configuration
public class ControllersConfig implements WebMvcConfigurer {

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/").setViewName("index");
 		registry.addViewController("/login").setViewName("login");
		registry.addViewController("/forbidden").setViewName("forbidden");
//		registry.addViewController("/addCoin").setViewName("addCoin");
//		registry.addViewController("/addSource").setViewName("addSource");
//		registry.addViewController("/addConnection").setViewName("addConnection");
//		registry.addViewController("/editCoin").setViewName("editCoin");
//		registry.addViewController("/editSource").setViewName("editSource");
//		registry.addViewController("/findSource").setViewName("findSource");
//		registry.addViewController("/delCoin").setViewName("delCoin");
//		registry.addViewController("/delSource").setViewName("delSource");
//		registry.addViewController("/delConnection").setViewName("delConnection");
	}
	
	//Set path for messages
    @Bean
    public MessageSource messageSource() {
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
        messageSource.setBasename("i18n/messages");
        messageSource.setDefaultEncoding("UTF-8");
        return messageSource;
    }
    
    //Set default locale
    @Bean
    public LocaleResolver localeResolver() {
        SessionLocaleResolver slr = new SessionLocaleResolver();
        slr.setDefaultLocale(Locale.ENGLISH);
        return slr;
    }
    
    //Set parameter for locale
    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor() {
        LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
        lci.setParamName("lang");
        return lci;
    }

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(localeChangeInterceptor());
	}
	
}
