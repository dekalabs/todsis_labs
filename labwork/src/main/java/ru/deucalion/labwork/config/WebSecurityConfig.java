package ru.deucalion.labwork.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import ru.deucalion.labwork.security.LoggedAccessDeniedHandler;
import ru.deucalion.labwork.security.db.UserBase;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired 
	UserBase base;
	
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
            .antMatchers("/js/**")
            .permitAll()
        .and()
        	.formLogin()
        		.loginPage("/login")
        		.failureUrl("/login?error")
        		.usernameParameter("username")
        		.passwordParameter("pass")
        	.permitAll()
        .and()
        	.logout()
        		.logoutUrl("/logout")
        		.logoutSuccessUrl("/login?logout")
        	.permitAll()
        .and()
        	.authorizeRequests()
        	.antMatchers("/")
        	.permitAll()
        .and()
        	.authorizeRequests()
        	.antMatchers(HttpMethod.PUT, "/rest/**")
        	.hasAuthority("ADMIN")
        .and()
        	.authorizeRequests()
        	.antMatchers(HttpMethod.DELETE, "/rest/**")
        	.hasAuthority("ADMIN")
        .and()
	        .authorizeRequests()
	    	.antMatchers(HttpMethod.GET, "/rest/**")
	    	.hasAnyAuthority("ADMIN", "USER")
	    .and()
	    	.authorizeRequests()
	    	.antMatchers("/add*")
	    	.hasAuthority("ADMIN")
    	.and()
	    	.authorizeRequests()
	    	.antMatchers("/edit*")
	    	.hasAuthority("ADMIN")
	    .and()
	    	.authorizeRequests()
	    	.antMatchers("/del*")
	    	.hasAuthority("ADMIN")
	    .and()
	    	.authorizeRequests()
	    	.antMatchers("/find*")
	    	.hasAnyAuthority("ADMIN", "USER")
        .and()
        	.exceptionHandling()
        		.accessDeniedHandler(new LoggedAccessDeniedHandler());
    }

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(base).passwordEncoder(new BCryptPasswordEncoder());
	}
}
