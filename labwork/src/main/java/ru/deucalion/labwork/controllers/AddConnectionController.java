package ru.deucalion.labwork.controllers;

import java.util.LinkedList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ru.deucalion.labwork.db.Coin;
import ru.deucalion.labwork.db.CoinSource;
import ru.deucalion.labwork.db.DataBase;

@Controller
@RequestMapping("/addConnection")
public class AddConnectionController {
	
	@Autowired
	DataBase base;
	
	@ModelAttribute("coinList")
	public LinkedList<Coin> getCoinLists()
	{
		return base.getCoins();
	}
	
	@ModelAttribute("sourceList")
	public LinkedList<CoinSource> getCoinSources()
	{
		return base.getCoinSources();
	}
	
	@GetMapping("")
	public String show()
	{
		return "addConnection";
	}
	
	@PostMapping("")
	public String add(
			@RequestParam(name = "coinID", required = true) Integer coinID,
			@RequestParam(name = "sourceID", required = true) Integer sourceID,
			ModelMap map)
	{
		map.addAttribute("coinID", coinID);
		map.addAttribute("sourceID", sourceID);
		
		boolean coinIDOk = coinID != -1;
		boolean sourceIDOk = sourceID != -1;
		boolean failed = !coinIDOk || !sourceIDOk;
		
		if(!coinIDOk)
		{
			map.addAttribute("coinIDError", true);
		}
		if(!sourceIDOk)
		{
			map.addAttribute("sourceIDError", true);
		}
		
		if(failed)
		{
			map.addAttribute("statusNotAdded", true);
		}
		else
		{
			base.addConnection(coinID, sourceID);
			map.addAttribute("statusAdded", true);
		}
		
		return "addConnection";
	}
}
