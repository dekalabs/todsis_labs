package ru.deucalion.labwork.controllers;

import java.util.LinkedList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ru.deucalion.labwork.db.Coin;
import ru.deucalion.labwork.db.DataBase;

@Controller
@RequestMapping("/findSource")
public class FindCoinSourceController {

	@Autowired
	DataBase base;
	
	@ModelAttribute("coinList")
	public LinkedList<Coin> getCoinLists()
	{
		return base.getCoins();
	}
	
	@GetMapping("")
	public String show()
	{
		return "findSource";
	}
	
	@PostMapping("")
	public String showResults(@RequestParam(name = "coinID", required = false) Integer id, ModelMap map)
	{		
		if(id == -1) {
			map.addAttribute("coinIDError", true);
			return "findSource";
		}
		
		Object results = null;
		
		results = base.getSourcesForCoin(id);
		
		map.addAttribute("isSearching", true);
		map.addAttribute("results", results);
		map.addAttribute("coinID", id);
		
		return "findSource";
	}
}
