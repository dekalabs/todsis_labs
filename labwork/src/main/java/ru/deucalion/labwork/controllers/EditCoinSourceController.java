package ru.deucalion.labwork.controllers;

import java.util.LinkedList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ru.deucalion.labwork.db.CoinSource;
import ru.deucalion.labwork.db.DataBase;

@Controller
@RequestMapping("/editSource")
public class EditCoinSourceController {

	@Autowired
	DataBase base;
	
	@ModelAttribute("sourceList")
	public LinkedList<CoinSource> getCoinSources()
	{
		return base.getCoinSources();
	}
	
	@GetMapping("")
	public String show()
	{
		return "editSource";
	}
	
	@PostMapping("")
	public String edit(
			@RequestParam(name = "sourceID", required = true) Integer id,
			@RequestParam(name = "coinSource", required = false) String address,
			ModelMap map)
	{
		map.addAttribute("sourceID", id);
		map.addAttribute("coinSource", address);
		
		if(id == -1)
		{
			map.addAttribute("sourceIDError", true);
			map.addAttribute("statusNotEdited", true);
			return "editSource";
		}
		
		boolean edited = base.updateCoinSource(id, address);
		if(edited)
		{
			map.addAttribute("statusEdited", true);
			map.addAttribute("sourceList", getCoinSources());
		}
		else
		{
			map.addAttribute("statusNotEdited", true);
		}
		
		return "editSource";
	}
}
