package ru.deucalion.labwork.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ru.deucalion.labwork.db.CoinSource;
import ru.deucalion.labwork.db.DataBase;

@Controller
@RequestMapping("/addSource")
public class AddSourceController {

	@Autowired
	DataBase base;
	
	@GetMapping("")
	public String show()
	{
		return "addSource";
	}
	
	@PostMapping("")
	public String add(
			@RequestParam(name = "coinSource", required = true) String address,
			ModelMap map)
	{
		boolean addressOk = CoinSource.validateAddress(address);
		
		map.addAttribute("coinSource", address);
		
		if(addressOk)
		{
			base.addCoinSource(address);
			map.addAttribute("statusAdded", true);
		}
		else
		{
			map.addAttribute("statusNotAdded", true);
			map.addAttribute("addressError", true);
		}
		
		return "addSource";
	}
}
