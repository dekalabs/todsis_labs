package ru.deucalion.labwork.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ru.deucalion.labwork.db.Coin;
import ru.deucalion.labwork.db.DataBase;

@Controller
@RequestMapping("/addCoin")
public class AddCoinController {

	@Autowired
	DataBase base;
	
	@GetMapping("")
	public String show()
	{
		return "addCoin";
	}
	
	@PostMapping("")
	public String add(
			@RequestParam(name = "coinName", required = true) String name,
            @RequestParam(name = "coinCountry", required = true) String country,
            @RequestParam(name = "coinYear", required = true) Integer year,
            ModelMap map)
	{
		boolean nameOk = Coin.validateName(name);
		boolean countryOk = Coin.validateCountry(country);
		boolean failed = !nameOk || !countryOk;
		
		map.addAttribute("coinName", name);
		map.addAttribute("coinCountry", country);
		map.addAttribute("coinYear", year);

		
		if(!nameOk)
		{
			map.addAttribute("nameError", true);
		}
        if(!countryOk)
        {
			map.addAttribute("countryError", true);
        }
        
        if(failed)
        {
        	map.addAttribute("statusNotAdded", true);
        }
        else
        {
        	map.addAttribute("statusAdded", true);
        	base.addCoin(name, country, year);

        }
        
        return "addCoin";
	}
}
