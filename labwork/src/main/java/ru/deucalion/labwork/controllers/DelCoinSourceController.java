package ru.deucalion.labwork.controllers;

import java.util.LinkedList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ru.deucalion.labwork.db.CoinSource;
import ru.deucalion.labwork.db.DataBase;

@Controller
@RequestMapping("/delSource")
public class DelCoinSourceController {

	@Autowired
	DataBase base;
	
	@ModelAttribute("sourceList")
	public LinkedList<CoinSource> getCoinSources()
	{
		return base.getCoinSources();
	}
	
	@GetMapping("")
	public String show()
	{
		return "delSource";
	}
	
	@PostMapping("")
	public String del(
			@RequestParam(name = "sourceID", required = true) Integer id,
			ModelMap map)
	{
		map.addAttribute("sourceID", id);
		
		boolean sourceIDOk = id != -1;
		
		if(sourceIDOk)
		{
			base.deleteCoinSource(id);
			map.addAttribute("statusDeleted", true);
			//Force update
			map.addAttribute("sourceList", getCoinSources());
		}
		else
		{
			map.addAttribute("statusNotDeleted", true);
			map.addAttribute("sourceIDError", true);
		}
		
		return "delSource";
	}
}
