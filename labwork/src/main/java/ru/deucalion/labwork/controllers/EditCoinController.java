package ru.deucalion.labwork.controllers;

import java.util.LinkedList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ru.deucalion.labwork.db.Coin;
import ru.deucalion.labwork.db.DataBase;

@Controller
@RequestMapping("/editCoin")
public class EditCoinController {

	@Autowired
	DataBase base;
	
	@ModelAttribute("coinList")
	public LinkedList<Coin> getCoinLists()
	{
		return base.getCoins();
	}
	
	@GetMapping("")
	public String show()
	{
		return "editCoin";
	}
	
	@PostMapping("")
	public String add(
			@RequestParam(name = "coinID", required = true) Integer id,
			@RequestParam(name = "coinName", required = false) String name,
            @RequestParam(name = "coinCountry", required = false) String country,
            @RequestParam(name = "coinYear", required = false) Integer year,
            ModelMap map)
	{

		map.addAttribute("coinID", id);
		map.addAttribute("coinName", name);
		map.addAttribute("coinCountry", country);
		map.addAttribute("coinYear", year);
		
		if(id == -1)
		{
			map.addAttribute("coinIDError", true);
			map.addAttribute("statusNotEdited", true);
			return "editCoin";
		}
		


		boolean edited = base.updateCoin(id, name, country, year);
		if(edited)
		{
			map.addAttribute("statusEdited", true);
			map.addAttribute("coinList", getCoinLists());
		}
		else
		{
			map.addAttribute("statusNotEdited", true);
		}

        
        
        return "editCoin";
	}
}
