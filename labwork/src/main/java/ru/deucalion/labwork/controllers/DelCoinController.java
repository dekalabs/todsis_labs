package ru.deucalion.labwork.controllers;

import java.util.LinkedList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ru.deucalion.labwork.db.Coin;
import ru.deucalion.labwork.db.DataBase;

@Controller
@RequestMapping("/delCoin")
public class DelCoinController {

	@Autowired
	DataBase base;
	
	@ModelAttribute("coinList")
	public LinkedList<Coin> getCoinLists()
	{
		return base.getCoins();
	}
	
	@GetMapping("")
	public String show()
	{
		return "delCoin";
	}
	
	@PostMapping("")
	public String del(
			@RequestParam(name = "coinID", required = false) Integer id, 
			ModelMap map)
	{
		map.addAttribute("coinID", id);
		
		boolean coinIDOk = id != -1;
		
		if(coinIDOk)
		{
			base.deleteCoin(id);
			map.addAttribute("statusDeleted", true);
			//Force update
			map.addAttribute("coinList", getCoinLists());
		}
		else
		{
			map.addAttribute("statusNotDeleted", true);
			map.addAttribute("coinIDError", true);
		}
		return "delCoin";
	}

}
